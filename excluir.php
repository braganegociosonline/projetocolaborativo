<?php
try{
    $pdo = new PDO("mysql:dbname=formulario;host=localhost", "root", "");
}catch(PDOException $e){
    echo "ERRO: ".$e->getMessage();
    exit;
}

$id = $_GET['id'];

$sql = "DELETE FROM formulario WHERE id = :id";
$sql = $pdo->prepare($sql);
$sql->bindValue(":id", $id);

if($sql->execute()){
    header("Location: index.php");
}else{
    echo "Erro ao excluir";
}
?>