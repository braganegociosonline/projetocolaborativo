<?php
try{
    $pdo = new PDO("mysql:dbname=formulario;host=localhost", "root", "");
}catch(PDOException $e){
    echo "ERRO: ".$e->getMessage();
    exit;
}

$resultado = '';

if(isset($_POST['nome'])){
    $nome = addslashes($_POST['nome']);
    $email = addslashes($_POST['email']);
    $telefone = addslashes($_POST['telefone']);
    $mensagem = addslashes($_POST['mensagem']);

    $sql = "INSERT INTO formulario SET nome = :nome, email = :email, telefone = :telefone, mensagem = :mensagem";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":nome", $nome);
    $sql->bindValue(":email", $email);
    $sql->bindValue(":telefone", $telefone);
    $sql->bindValue(":mensagem", $mensagem);
    
    if($sql->execute()){
        $resultado = 'sucesso';
    }else{
        $resultado = 'erro';
    }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Formlário de cadastro</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <h1 class="text-center">Formulário de Cadastro</h1>
      <div class="container-fluid mt-5">
          <div class="row">
              <div class="col-md-12">
                <?php if($resultado == 'sucesso'): ?>
                    <div class="alert alert-success">
                        <p>Cadastrado com sucesso!</p>
                    </div>
                <?php elseif($resultado == 'erro'): ?>
                    <div class="alert alert-danger">
                        <p>Erro ao cadastrar!</p>
                    </div>
                <?php else: ?>

                <?php endif; ?>
              </div>
              <div class="col-md-12">
                <form action="" method="post">
                    <div class="form-group"><input type="text" name="nome" id="" placeholder="Nome" class="form-control"></div>
                    <div class="form-group"><input type="text" name="email" id="" placeholder="Email" class="form-control"></div>
                    <div class="form-group"><input type="text" name="telefone" id="" placeholder="Telefone" class="form-control"></div>
                    <div class="form-group"><textarea name="mensagem" id="" cols="30" placeholder="Mensagem" rows="10" class="form-control"></textarea></div>
                    <div class="form-group"><input type="submit" value="Enviar" class="btn btn-danger"></div>
                </form>
              </div>
          </div>
      </div>

      <div class="container-fluid mt-5">
          <div class="row">
              <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Telefone</th>
                            <th>Mensagem</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $sql1 = "SELECT * FROM formulario";
                    $sql1 = $pdo->prepare($sql1);
                    $sql1->execute();

                    while($dados = $sql1->fetch(PDO::FETCH_ASSOC)){
                    ?>
                        <tr>
                            <td scope="row"><?= $dados['nome']; ?></td>
                            <td><?= $dados['email']; ?></td>
                            <td><?= $dados['telefone']; ?></td>
                            <td><?= $dados['mensagem']; ?></td>
                            <td><a href="editar.php?id=<?= $dados['id']; ?>" class="btn btn-primary">Editar</a> | <a href="excluir.php?id=<?= $dados['id']; ?>" class="btn btn-danger">Excluir</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>