<?php
try{
    $pdo = new PDO("mysql:dbname=formulario;host=localhost", "root", "");
}catch(PDOException $e){
    echo "ERRO: ".$e->getMessage();
    exit;
}

$id = $_GET['id'];

if(isset($_POST['nome'])){
    $nome = addslashes($_POST['nome']);
    $email = addslashes($_POST['email']);
    $telefone = addslashes($_POST['telefone']);
    $mensagem = addslashes($_POST['mensagem']);

    $sql = "UPDATE formulario SET nome = :nome, email = :email, telefone = :telefone, mensagem = :mensagem WHERE id = :id";
    $sql = $pdo->prepare($sql);
    $sql->bindValue(":nome", $nome);
    $sql->bindValue(":email", $email);
    $sql->bindValue(":telefone", $telefone);
    $sql->bindValue(":mensagem", $mensagem);
    $sql->bindValue(":id", $id);
    
    if($sql->execute()){
        header("Location: index.php");
    }else{
        echo "Erro ao alterar";
    }
}

$query = "SELECT * FROM formulario WHERE id = :id";
$query = $pdo->prepare($query);
$query->bindValue(":id", $id);
$query->execute();

$dados = $query->fetch(PDO::FETCH_ASSOC);
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Formlário de cadastro</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <h1 class="text-center">Editar Usuário</h1>
      <div class="container-fluid mt-5">
          <div class="row">
              <div class="col-md-12">
                <form action="" method="post">
                    <div class="form-group"><input type="text" name="nome" placeholder="Nome" class="form-control" value="<?= $dados['nome']; ?>"></div>
                    <div class="form-group"><input type="text" name="email" placeholder="Email" class="form-control" value="<?= $dados['email']; ?>"></div>
                    <div class="form-group"><input type="text" name="telefone" placeholder="Telefone" class="form-control" value="<?= $dados['telefone']; ?>"></div>
                    <div class="form-group"><textarea name="mensagem" id="" cols="30" placeholder="Mensagem" rows="10" class="form-control"><?= $dados['mensagem']; ?></textarea></div>
                    <div class="form-group"><input type="submit" value="Enviar" class="btn btn-danger"> <a href="index.php" class="btn btn-primary">Voltar</a></div>
                </form>
              </div>
          </div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>